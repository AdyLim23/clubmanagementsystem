import React, { Component } from 'react';

export default class Success extends Component {
    render() {
        return (
            <div className="alert alert-success" role="alert">
                Record Updated Successfully
            </div>
        );
    }
}


