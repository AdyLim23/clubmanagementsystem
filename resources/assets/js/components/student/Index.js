import React, { Component } from 'react';
import{BrowserRouter as Router , Link , Route} from 'react-router-dom';
import Create from './Create';
import Listing from './Listing';
import Edit from './Edit';
import Show from './Show';

export default class Index extends Component {
    render() {
        return (
            <div className="container">
                <Router>
                	<div>

                		<Link to="/student" style={{paddingRight:"10px"}} className="btn btn-primary">Listing</Link>
                		<Link to="/student/create" className="btn btn-primary" style={{marginLeft:"10px"}}>Create</Link>

                		<Route exact path="/student" component={Listing} />
                		<Route exact path="/student/create" component={Create} />
                		<Route exact path="/student/edit/:id" component={Edit} />
                        <Route exact path="/student/show/:id" component={Show} />
                	</div>
                </Router>
            </div>
        );
    }
}


