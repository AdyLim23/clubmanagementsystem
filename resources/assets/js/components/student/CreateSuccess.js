import React, { Component } from 'react';

export default class CreateSuccess extends Component {
    render() {
        return (
            <div className="alert alert-success" role="alert">
                Record Added Successfully
            </div>
        );
    }
}


